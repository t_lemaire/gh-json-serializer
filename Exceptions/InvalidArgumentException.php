<?php

namespace GorillaHub\JSONSerializerBundle\Exceptions;

use \GorillaHub\JSONSerializerBundle\JSONSerializerException;

class InvalidArgumentException extends JSONSerializerException
{

}