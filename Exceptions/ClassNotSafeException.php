<?php

namespace GorillaHub\JSONSerializerBundle\Exceptions;

use \GorillaHub\JSONSerializerBundle\JSONSerializerException;

class ClassNotSafeException extends JSONSerializerException
{

}