<?php

namespace GorillaHub\JSONSerializerBundle\Exceptions;

use \GorillaHub\JSONSerializerBundle\JSONSerializerException;

class ClassNotFoundException extends JSONSerializerException
{

}