<?php

namespace GorillaHub\JSONSerializerBundle\Tests;

use \GorillaHub\JSONSerializerBundle\JSONSerializer;
use \GorillaHub\JSONSerializerBundle\Tests\Person;

class JSONSerializerTest  extends \PHPUnit_Framework_TestCase
{

	public function testSerializingString()
	{
		$serializer = new JSONSerializer();

		$parameter = 'Hello World!';

		$expected = json_encode($parameter, JSON_PRETTY_PRINT);

		$this->assertEquals($expected, $serializer->serialize($parameter));
	}

    public function testDeserializingString(){
        $serializer = new JSONSerializer();
        $parameter = 'Yo !';

        $this->assertEquals($parameter, $serializer->deserialize($serializer->serialize($parameter)));

    }

	public function testSerializingInteger()
	{
		$serializer = new JSONSerializer();

		$parameter = 10;

		$expected = json_encode($parameter, JSON_PRETTY_PRINT);

		$this->assertEquals($expected, $serializer->serialize($parameter));
	}

	public function testSerializingSingleArray()
	{
		$serializer = new JSONSerializer();

		$parameter = array('Hello World!', 10);

		$expected = json_encode($parameter, JSON_PRETTY_PRINT);

		$this->assertEquals($expected, $serializer->serialize($parameter, null, true));
	}

	public function testSerializingAssocArray()
	{
		$serializer = new JSONSerializer();

		$parameter = array(
			'string' => 'Hello World!',
			'integer' => 10
		);

		$expected = json_encode($parameter, JSON_PRETTY_PRINT);

		$this->assertEquals($expected, $serializer->serialize($parameter, null, true));
	}

	public function testSerializingMultidimensionalArray()
	{
		$serializer = new JSONSerializer();

		$parameter = array(
			'strings' => array(
				'first' => 'Hello',
				'second' => 'World1'
			),

			'integers' => array(
				10,
				20
			)
		);

		$expected = json_encode($parameter, JSON_PRETTY_PRINT);

		$this->assertEquals($expected, $serializer->serialize($parameter, null, true));
	}

	public function testSerializingSimpleObjectDefinition()
	{
		$serializer = new JSONSerializer();

		$parameter = new DummyObject();
		$parameter->setName('Dummy');

        $serialized = $serializer->serialize($parameter);
        //decode it
        $decoded = json_decode($serialized);
        $this->assertEquals($parameter->getName(),$decoded->name);

		$this->assertEquals(get_class($parameter), $decoded->{'@type'}[0]);
	}

	public function testSerializingAnArrayContainingSimpleObjects()
	{
		$serializer = new JSONSerializer();

		$dummyObject1 = new DummyObject();
		$dummyObject1->setName('Dummy1');

		$dummyObject2 = new DummyObject();
		$dummyObject2->setName('Dummy2');

		$parameter = array(
			$dummyObject1,
			$dummyObject2
		);

        $serialized = $serializer->serialize($parameter);
        $deserialized = $serializer->deserialize($serialized);
        $this->assertEquals($parameter, $deserialized);
	}

	public function testSerializingSimpleObjectDefinitionContainingAnArray()
	{
		$serializer = new JSONSerializer();

		$parameter = new DummyObjectContainingAnArray();
		$parameter->setArray(
			array(
				'strings' => array(
					'first' => 'Hello',
					'second' => 'World1'
				),

				'integers' => array(
					10,
					20
				)
			)
		);


        $serialized = $serializer->serialize($parameter);
        $deserialized = $serializer->deserialize($serialized);
        $this->assertEquals($parameter, $deserialized);

	}

	public function testSerializingComplexObjects()
	{
		$serializer = new JSONSerializer();

		$array = array(
			'strings' => array(
				'first' => 'Hello',
				'second' => 'World1'
			),

			'integers' => array(
				10,
				20
			)
		);

		$dummyObject = new DummyObjectContainingAnArray();
		$dummyObject->setArray($array);

		$firstObject = new ComplexObject();
		$firstObject->setName('First');
		$firstObject->setAge(10);
		$firstObject->setArray($array);
		$firstObject->setSon($dummyObject);

        $serialized = $serializer->serialize($firstObject);
        $deserialized = $serializer->deserialize($serialized);
        $this->assertEquals($firstObject, $deserialized);
	}


    public function  testSerializedArrayCOntainingOnlyAStringUnderObjectMethod(){

        $mainObject = new DummyObject();
        $mainObject->setName(array('String in array'));

        $serializer = new JSONSerializer();
        $serialized = $serializer->serialize($mainObject);
        $deserialized = $serializer->deserialize($serialized);
        $this->assertEquals($mainObject, $deserialized);

    }

    public function testSerializingObjectContainingArrayOfObject(){
        $mainObject = new DummyObject();

        $object1 = new DummyObject();
        $object2 = new DummyObject();
        $object3 = new DummyObject();

        $arrayOfObject = array($object1, $object2, $object3);

        $mainObject->setName($arrayOfObject);

        $serializer = new JSONSerializer();

        $serialized = $serializer->serialize($mainObject);

        $deserialized = $serializer->deserialize($serialized);

        $this->assertEquals($mainObject, $deserialized);
    }


	public function testDeserializingAnObjectWithAPropertyThatDoesNotMatchCurrentClassDefinition()
	{
		$serializer = new JSONSerializer();

		$array = array(
			'strings' => array(
				'first' => 'Hello',
				'second' => 'World1'
			),

			'integers' => array(
				10,
				20
			)
		);

		$dummyObject = new DummyObjectContainingAnArray();
		$dummyObject->setArray($array);

		$complexObject = new ComplexObject();
		$complexObject->setName('First');
		$complexObject->setAge(10);
		$complexObject->setArray($array);
		$complexObject->setSon($dummyObject);

		$serialized = '{"@type":["GorillaHub\\\JSONSerializerBundle\\\Tests\\\ComplexObject",[]],"weight":"100kg","age":10,"array":{"strings":{"first":"Hello","second":"World1"},"integers":[10,20]},"son":{"@type":["GorillaHub\\\JSONSerializerBundle\\\Tests\\\DummyObjectContainingAnArray",[]],"array":{"strings":{"first":"Hello","second":"World1"},"integers":[10,20]}},"secondSon":{"@type":["GorillaHub\\\JSONSerializerBundle\\\Tests\\\DummyObjectContainingAnArray",[]],"array":{"strings":{"first":"Hello","second":"World1"},"integers":[10,20]}},"name":"First"}';

		$this->assertEquals($complexObject, $serializer->deserialize($serialized));
	}


}


class DummyObject
{
    private $name;

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

}

class DummyObjectContainingAnArray
{
	private $array = array();

	/**
	 * @param array $array
	 */
	public function setArray($array)
	{
		$this->array = $array;
	}

	/**
	 * @return array
	 */
	public function getArray()
	{
		return $this->array;
	}

}

class ComplexObject
{
	private $array = array();

	private $name;

	private $age;

	private $son;

	/**
	 * @param mixed $age
	 */
	public function setAge($age)
	{
		$this->age = $age;
	}

	/**
	 * @return mixed
	 */
	public function getAge()
	{
		return $this->age;
	}

	/**
	 * @param array $array
	 */
	public function setArray($array)
	{
		$this->array = $array;
	}

	/**
	 * @return array
	 */
	public function getArray()
	{
		return $this->array;
	}

	/**
	 * @param mixed $son
	 */
	public function setSon(DummyObjectContainingAnArray $son)
	{
		$this->son = $son;
	}

	/**
	 * @return mixed
	 */
	public function getSon()
	{
		return $this->son;
	}

	/**
	 * @param mixed $name
	 */
	public function setName($name)
	{
		$this->name = $name;
	}

	/**
	 * @return mixed
	 */
	public function getName()
	{
		return $this->name;
	}

}
