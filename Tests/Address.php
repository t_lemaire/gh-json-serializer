<?php
namespace GorillaHub\JSONSerializerBundle\Tests;


/**
 * Class Address
 * @package GorillaHub\JSONSerializerBundle\Tests
 */
class Address {
	/**
	 * @var string
	 */
	private $door;
	/**
	 * @var string
	 */
	private $street;
	/**
	 * @var string
	 */
	private $city;

	/**
	 * @param string $city
	 */
	public function setCity($city) {
		$this->city = $city;
	}

	/**
	 * @return string
	 */
	public function getCity() {
		return $this->city;
	}

	/**
	 * @param string $door
	 */
	public function setDoor($door) {
		$this->door = $door;
	}

	/**
	 * @return string
	 */
	public function getDoor() {
		return $this->door;
	}

	/**
	 * @param string $street
	 */
	public function setStreet($street) {
		$this->street = $street;
	}

	/**
	 * @return string
	 */
	public function getStreet() {
		return $this->street;
	}
}