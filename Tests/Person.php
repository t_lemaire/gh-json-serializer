<?php

namespace GorillaHub\JSONSerializerBundle\Tests;

/**
 * @Annotation
 */
class Person
{
	/**
	 * @var int $id
	 */
	private $id;
	/**
	 * @var string $name
	 */
	private $name;
	/**
	 * @var string $phoneNumber
	 */
	private $phoneNumber;
	/**
	 * @var Address $address
	 */
	private $address;
	/**
	 * @var string $sex
	 */
	private $sex;
	/**
	 * @var string $occupation
	 */
	private $occupation;

	/**
	 * @param Address $address
	 */
	public function setAddress($address)
	{
		$this->address = $address;
	}

	/**
	 * @return Address
	 */
	public function getAddress()
	{
		return $this->address;
	}

	/**
	 * @param int $id
	 */
	public function setId($id)
	{
		$this->id = $id;
	}

	/**
	 * @return int
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param string $name
	 */
	public function setName($name)
	{
		$this->name = $name;
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * @param string $occupation
	 */
	public function setOccupation($occupation)
	{
		$this->occupation = $occupation;
	}

	/**
	 * @return string
	 */
	public function getOccupation()
	{
		return $this->occupation;
	}

	/**
	 * @param string $phoneNumber
	 */
	public function setPhoneNumber($phoneNumber)
	{
		$this->phoneNumber = $phoneNumber;
	}

	/**
	 * @return string
	 */
	public function getPhoneNumber()
	{
		return $this->phoneNumber;
	}

	/**
	 * @param string $sex
	 */
	public function setSex($sex)
	{
		$this->sex = $sex;
	}

	/**
	 * @return string
	 */
	public function getSex()
	{
		return $this->sex;
	}

	/**
	 * This method only exists to test a condition for PHPUnit
	 * @codeCoverageIgnore
	 *
	 * @param $now
	 */
	public function getLost($now)
	{
	}

}
