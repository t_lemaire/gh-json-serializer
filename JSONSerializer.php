<?php

namespace GorillaHub\JSONSerializerBundle;

use \GorillaHub\JSONSerializerBundle\Exceptions\ClassNotFoundException;
use GorillaHub\JSONSerializerBundle\Exceptions\ClassNotSafeException;
use \GorillaHub\JSONSerializerBundle\Exceptions\InvalidArgumentException;

class JSONSerializer
{
	/** @var bool */
	private $safeMode;

	/** @var LoggerInterface */
	private $logger;

	/**
	 * @param bool $safeMode True if only recognized objects will be created, false if any objects can be created.
	 * @param LoggerInterface|null $logger A logger to which to publish warnings, or null to not log anything.
	 */
	public function __construct($safeMode = true, LoggerInterface $logger = null) {
		$this->safeMode = $safeMode;
		$this->logger = $logger;
	}

    /**
     * In order for the object to be serialized, it must implement getters and setters for the fields that will be serialized
     * If you are using safemode (on by default), classes must either be in the whitelist or implement the SafeToSerializeInterface.
     * @param $object
     * @param JSONFieldSkipper|null $skipper
     * @param bool $json_pretty_print
     * @return string
     */
	public function serialize($object, JSONFieldSkipper $skipper = null, $json_pretty_print = false)
	{
		if (defined('JSON_PRETTY_PRINT') && $json_pretty_print == true) {
			return json_encode($this->extractData($object, $skipper), JSON_PRETTY_PRINT);
		} else {
			return json_encode($this->extractData($object, $skipper));
		}
	}

	/**
	 * @param object|string $json A string to decode, or an already decoded object.
	 * @param bool|null $safeMode True if only recognized objects will be created, false if any objects can be
	 * 		created, or null to use the default setting passed through the constructor.
	 * @param LoggerInterface|null $logger A logger to which to publish warnings, or null to use the default logger
	 * 		passed through the constructor.
	 * @return array|object The deserialized array or object, or the value passed through $json if $json is an object.
	 * @throws ClassNotSafeException
	 * @throws ClassNotFoundException
	 */
	public function deserialize($json, $safeMode = null, LoggerInterface $logger = null)
	{
		$logger = ($logger !== null) ? $logger : $this->logger;
		$safeMode = ($safeMode !== null) ? $safeMode : $this->safeMode;
		if (is_object($json)) {
			return $json;
		}

        $jsonDecoded = json_decode($json, true);

        if(!is_array($jsonDecoded)){
            return $jsonDecoded;
        }

        if (!isset($jsonDecoded['@type'])) {
            //it's a simple array for the beginning
            return $this->convertToArray($jsonDecoded, $safeMode, $logger);
        }

		return $this->convertToObject($jsonDecoded, $safeMode, $logger);
	}

	/**
	 * Take the Json data and convert it in array
	 *
	 * @param object|array $jsonData
	 * @param bool $safeMode
	 * @param LoggerInterface|null $logger
	 * @return array
	 */
    private function convertToArray($jsonData, $safeMode = false, LoggerInterface $logger = null){
        $t = array();
        foreach ($jsonData as $key => $o) {
            if (is_array($o) && isset($o['@type'])) {
                $t[$key] = $this->convertToObject($o, $safeMode, $logger);
                continue;
            } else {
                $t[$key] = $o;
            }
        }
        return $t;
    }

	public function extractData($object, JSONFieldSkipper $skipper = null)
	{
		if ($object == null || $object == '') {
			return $object;
		}

        if(is_array($object)){
            return $this->fetchArray($object, $skipper);
        }else if(!is_object($object)) {
			return $object;
		}

        return $this->fetchClass($object, $skipper);
	}

    private function fetchArray($array, $skipper){

        $value = array();
        foreach ($array as $key => $o) {
            if(is_array($o)){
                $value[$key] = $this->fetchArray($o, $skipper);
            } elseif(is_object($o)){
                $value[$key] = $this->fetchClass($o, $skipper);
            } else {
                $value[$key] =  $o;
            }
        }

        return $value;
    }

    private function fetchClass($object, $skipper){
        if (get_class($object) == 'stdClass') {
            return $object;
        }

        $class = get_class($object);

        if (!class_exists($class)) {
            throw new ClassNotFoundException('Unable to locate class [' . $class . ']');
        }

        $reflectionClass = new \ReflectionClass($object);
        $data['@type']   = array(
            get_class($object),
            array(), // constructor arguments
        );

        foreach ($reflectionClass->getMethods(\ReflectionMethod::IS_PUBLIC) as $reflectionMethod) {
            if (strtolower(substr($reflectionMethod->getName(), 0, 3)) !== 'get') {
                continue;
            }

            if ($reflectionMethod->getNumberOfRequiredParameters() > 0) {
                continue;
            }

            $getMethodName = $reflectionMethod->getName();
            $setMethodName = $getMethodName;
            $setMethodName[0] = 's';
            if (method_exists($object, $setMethodName) === false) {
                continue;
            }
            $property = lcfirst(substr($getMethodName, 3));
            $value    = $reflectionMethod->invoke($object);

            if (is_array($value)) {
                $value = $this->fetchArray($value, $skipper);
            } elseif (is_object($value)) {
                $value = $this->fetchClass($value, $skipper);
            } else {
            }

            if ($skipper !== null) {
                if (true === $skipper->skip($property, $value)) {
                    $data[$property] = $value;
                } else {
                    continue;
                }
            }

            $data[$property] = $value;
        }

        return $data;
    }

    /**
     * Convert Json data into a complex object
	 *
	 * @param array $data
	 * @param bool $safeMode
	 * @param LoggerInterface|null $logger
	 * @throws ClassNotFoundException
	 * @throws ClassNotSafeException
	 * @return object
     */
	private function convertToObject($data, $safeMode, LoggerInterface $logger = null)
	{

		if (!isset($data['@type'][0])) {
			return array();
		}

		$class = $data['@type'][0];

		$reflectionClass = new \ReflectionClass($class);

		$constructorArguments = $data['@type'][1] ? : array();

		if ($this->isClassBlacklisted($reflectionClass)) {
			throw new ClassNotSafeException('Class [' . $class . '] is not a recognized SDK class.');
		}
		if ($this->isClassSafe($reflectionClass)) {
			$object = $reflectionClass->newInstanceArgs($constructorArguments);
		} else {
			if ($logger !== null) {
				$logger->warn('Class [' . $class . '] is not a recognized SDK class.');
			}
			if (!empty($constructorArguments)) {
				if ($safeMode) {
					throw new ClassNotSafeException('Class [' . $class . '] is not a recognized SDK class.');
				}
				$object = $reflectionClass->newInstanceArgs($constructorArguments);
			} else {
				if ($safeMode) {
					$object = $reflectionClass->newInstanceWithoutConstructor();
				} else {
					$object = $reflectionClass->newInstanceArgs($constructorArguments);
				}
			}
		}

		unset($data['@type']);

		foreach ($data as $property => $value) {
			$setter = 'set' . $property;
			if (method_exists($object, $setter)) {

				if (is_array($value) && $this->isAssoc($value)) {
					if (isset($value['@type'])) {
						$value = $this->convertToObject($value, $safeMode, $logger);
					} else {
						$value = $this->convertToArray($value, $safeMode, $logger);

					}
				} elseif (is_array($value)) {
					$t = array();
					foreach ($value as $o) {
						if(is_array($o)){
							if(isset($o['@type'])){
								$t[] = $this->convertToObject($o, $safeMode, $logger);
							} else {
								$t[] = $this->convertToArray($o, $safeMode,$logger);
							}
						} else {
							$t[] = $o;
						}
					}
					$value = $t;
				}

				if (!is_null($value)) {
					$object->$setter($value);
				}
			}
		}

		return $object;
	}

	private function isAssoc($arr)
	{
		return array_keys($arr) !== range(0, count($arr) - 1);
	}

	private function isClassSafe(\ReflectionClass $reflectionClass) {
	    if ($reflectionClass->implementsInterface(SafeToSerializeInterface::class)) {
	        return true;
        }

		while ($reflectionClass) {
			$name = $reflectionClass->getName();
			if ($this->isClassNameSafe($name) === false) {
				return false;
			}
			$reflectionClass = $reflectionClass->getParentClass();
		}
		return true;
	}

	private function isClassNameSafe($className) {
		$allowedClasses = array(
			'DateTime',
			'stdClass',
			'GorillaHub\FilesBundle\Domain\File',
			'GorillaHub\FilesBundle\Domain\Directory',
			'GorillaHub\FilesBundle\Domain\Path',
			'GorillaHub\FilesBundle\Domain\Node',
			'GorillaHub\SDKs\SDKBundle\V0001\ExceptionTransmitter'
		);
		if (in_array($className, $allowedClasses)) {
			return true;
		}
		$allowedClassRegexes = array(
			'`^GorillaHub\\\\SDKs\\\\.*\\\\Domain\\\\`',
			'`^MindGeek\\\\MediaInfoBundle\\\\Domain\\\\`'
		);
		foreach ($allowedClassRegexes as $allowedClassRegex) {
			if (preg_match($allowedClassRegex, $className) > 0) {
				return true;
			}
		}
		return false;
	}

	private function isClassBlacklisted(\ReflectionClass $reflectionClass) {
		while ($reflectionClass) {
			$name = $reflectionClass->getName();
			if ($this->isClassNameBlacklisted($name)) {
				return true;
			}
			$reflectionClass = $reflectionClass->getParentClass();
		}
		return false;
	}

	private function isClassNameBlacklisted($className) {
		$blockedClasses = array('OAuth2\\Storage\\Pdo', 'SoapClient', 'SimpleXMLElement', 'DirectoryIterator', 'mysql', 'mysqli');
		return in_array($className, $blockedClasses);
	}

}
