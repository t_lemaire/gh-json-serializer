<?php
namespace GorillaHub\JSONSerializerBundle;

/**
 * This is a logger that can be passed to the JSONSerializer that allows JSONSerializer to trigger user warnings.
 */
class PhpLogger implements LoggerInterface
{
	public function warn($message) {
		trigger_error($message, E_USER_WARNING);
	}
}