<?php
namespace GorillaHub\JSONSerializerBundle;

interface JSONFieldSkipper
{

	public function skip($property, $value);

}