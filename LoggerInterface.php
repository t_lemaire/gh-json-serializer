<?php
namespace GorillaHub\JSONSerializerBundle;

/**
 * An instance of this class can allow the JSONSerializer to publish warning messages.
 */
interface LoggerInterface
{
	public function warn($message);
}